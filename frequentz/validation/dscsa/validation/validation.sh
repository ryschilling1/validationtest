#!/bin/bash 
#
#QA Validation script, run this script FIRST
BITUSERNAME=$1;shift
#set up MQ environment
#. /opt/mqm/bin/setmqenv

# set IRIS home
if [ "$IRIS_HOME" != "" ]
then
        export TS_HOME=$IRIS_HOME
fi

# make logs dir
if [ ! -d logs ]
then
        mkdir -v logs
fi

USERNAME=iris
OUTPUT=logs/$0.out
ERROR=logs/$0.err

# collect first and last for git commit
echo "First and last name of executor"
read -p "Enter First Name: " firstname 
read -p "Enter Last Name: " lastname   	

# set customer for dir
echo "What customer?(as it is spelt in bitbucket, case sensitive)"
read customer

#set git URL
GIT_URL=https://$BITUSERNAME@bitbucket.org/ryschilling1/validationtest.git

# Enter the production IP
echo "Enter the prod host IP"
read PROD_HOST
SSH=$(echo ${USERNAME}@${PROD_HOST})
echo $SSH

#check to see if customer directory exists
if [ ! -d /home/iris/git/$customer ]
then
        git clone $GITURL
else
	git remote set-url origin $GIT_URL
fi

# set up bitbucket
#git clone
git config --global  "$firstname.$lastname@frequentz.com"
git config --global user.name "$firstname $lastname"
#echo "git remote set-url origin https://$1@bitbucket.org/ryschilling1/validationtest.git"

# navigate to customer dir
cd /home/iris/git/$customer

# add root of this dir to git
git init

# make the directories
if [ ! -d /qa_export ] 
then
	mkdir -v qa_export
fi
cd qa_export

if [ ! -d /tsar ]
then
	mkdir -v tsar
fi
if [ ! -d /master_data ]
then
	mkdir -v master_data
fi
cd tsar

# export tsar from QA
echo "Exporting tsar"
/opt/Frequentz/IRIS/bin/exportTSAR -file ./tsar.tsar
jar -xvf tsar.tsar
cd ../
cd master_data

# export master data from QA
echo "Exporting master data"
/opt/Frequentz/IRIS/bin/exportMasterData -outputDir=. -vocabularyType=all -hierarchyType=all 

# add new files to git and review then commit
echo "Adding tsar and master data to staging"
cd ../../
git remote add origin  https://$BITUSERNAME@bitbucket.org/ryschilling1/validationtest.git
git add qa_export/tsar/*
git add qa_export/master_data/*

git status
echo "Do you want to commit the changes?(Y/N)"
read  $i
if [[ $i != "N" ]]
then
        git commit --author "$firstname $lastname <$firstname.$lastname@frequentz.com>"
	git push origin master
        
fi 

# connect to production server and run the prod_validation script
echo "Connecting to production to run production validation"
ssh -o StrictHostKeyChecking=no  $SSH  "/home/iris/git/qms/frequentz/validation/dscsa/prod_validation.sh $customer $firstname $lastname"
echo "Validation script has been completed"
cat $ERROR
exit
