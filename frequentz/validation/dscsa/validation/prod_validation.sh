#!/bin/bash 
# Production Validation script

# parse the arguments
customer=$1;shift
firstname=$1;shift
lastname=$1;shift

# Set environment
. /opt/mqm/bin/setmqenv

if [ "$IRIS_HOME" != "" ]
then
        export TS_HOME=$IRIS_HOME
fi

if [ ! -d logs ]
then
        mkdir logs
fi

OUTPUT=logs/validation/$0.out
ERROR=logs/validation/$0.err
#set up git
git config --global  "$firstname.$lastname@frequentz.com"
git config --global user.name "$firstname $lastname"
git remote set-url origin https://$BITUSERNAME@bitbucket.org/ryschilling1/validationtest.git

#if customer dir doesn't exist create it
if [ ! -d /home/iris/git/customer]
then
        git clone https://$BITUSERNAME@bitbucket.org/ryschilling1/validationtest.git
        cd /home/iris/git/$customer;
else
        cd /home/iris/git/$customer
fi

# pull updates from git
git pull
echo "pulling updates from BitBucket"

# run the deploy script with qa params
echo "Running deploy.sh"
./deploy.sh -qa

# add the logs to git
echo "Adding logs to git"
git add logs/production/prod_import_deploy.*
git status
echo "Do you want to commit the changes?(Y/N)"
read $i
if [[ $i != "N" ]]
then
        git commit --author "$firstname $lastname <$firstname.$lastname@frequentz.com>"
fi 

# make a dirs for production exports from IRIS
if [ ! -d /prod_export ]
then
	mkdir prod_export
fi
cd prod_export
if [ ! -d /tsar ]
then
	mkdir tsar
fi
if [ ! -d /master_data ]
then
	mkdir master_data
fi
cd tsar

# export tsar
echo "exporting TSAR"
exportTSAR -file tsar.tsar
jar -xvf tsar.tsar
rm tsar.tsar

# Export all master data into the "master_data" directory:
cd ../master_data
echo "exporting master data"
exportMasterData -outputDir=/home/iris/git/$customer/prod_export/master_data -vocabularyType=all -hierarchyType=all 
git add ../tsar
git add master_data
git status
echo "Do you want to commit the changes?(Y/N)"
read  $i
if [[ $i != "N" ]]
then
        git commit --author "$firstname $lastname <$firstname.$lastname@frequentz.com>"
fi 
# You'll be prompted to enter a commit comment. Use something like "Export from Breckenridge Production instance."
#Compare the export from QA to the export from Production using the diff command. Redirect the result to a file prod_export/directory_tree_diff.out:
diff --brief -Nr qa_export/ prod_export/ > prod_export/directory_tree_diff.out
echo "Perform individual comparisons on any files listed to find out if there are real differences or if the differences are only related to changed internal (database) IDs or different ordering. Real differences indicate potential problems and should be addressed."
sleep 10s
echo "To view outside of this script open prod_export/directory_tree_diff.out"
# Echo the file to view the differences
cat  prod_export/directory_tree_diff.out
# Stage the "directory_tree_diff.out" file
# Commit the current changes using your User ID
echo "Do you want to stage and commit the changes?(Y/N"
read  $i
if [[ $i != "N" ]]
then
	git add prod_export/directory_tree_diff.out
	git commit --author "$firstname $lastname <$firstname.$lastname@frequentz.com>"
	git push
fi
cat $ERROR
exit

