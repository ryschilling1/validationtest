#!/bin/bash

WAS_ADMIN_PW=was4me
WS_USER=user1
WS_PASSWORD=u7i8o9p0
EVENTQ=myeventq

submitEvent() {
   $IRIS_HOME/bin/submitEvent -url https://$(hostname):9443/ts/engine/HTTPCaptureBinding -user $WS_USER -password $WS_PASSWORD $1
}

# TSAR
echo "Deploying TSAR..."
cd tsar
zip -9 -r dscsa_ser_sample.tsar *
cd ..
$IRIS_HOME/bin/importReplaceTSAR -file tsar/dscsa_ser_sample.tsar -P=$WAS_ADMIN_PW
# Start capture endpoints
$IRIS_HOME/bin/startIRIS -S=capture -P=$WAS_ADMIN_PW

# Import master data
echo "Importing master data..."
$IRIS_HOME/bin/importMasterData $IRIS_HOME/samples/dscsa_sample/master_data/*.xml
$IRIS_HOME/bin/importMasterData dispenser/internal_masterdata/locations.xml

sleep 15

# Submit events
echo "Submitting valid DSCSA events..."

# Wholesaler 1
submitEvent wholesaler1/downstream_events/21000010cp.xml

# Wholesaler 2
submitEvent wholesaler2/downstream_events/71000010cp.xml

# Dispenser
submitEvent dispenser/internal_events/30000010rv-2.xml
submitEvent dispenser/internal_events/80000010rv-2.xml

echo "Submitting events that will cause failures. Failures are intentional and can be ignored."

# Submit some invalid events to cause failed events
submitEvent wholesaler1/downstream_events/21000010cp.xml
submitEvent invalid_events/21000010cp.xml
