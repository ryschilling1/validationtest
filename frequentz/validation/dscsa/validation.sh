  #!/bin/bash 
# QA Validation script
. /opt/mqm/bin/setmqenv
if [ "$IRIS_HOME" != "" ]
then
        export TS_HOME=$IRIS_HOME
fi

if [ ! -d logs ]
then
        mkdir -v logs
fi


USERNAME="iris"
OUTPUT=logs/$0.out
ERROR=logs/$0.err
SSH=$(echo ${$USERNAME}@${PROD_HOST})

# collect first and last for git commit
echo "First and last name of executor"
while [[$lastname == '']] || [[ $firstname == '' ]] 
do
	read -p "Enter First Name: " firstname 
	read -p "Enter Last Name: " lastname   
	echo "Enter a valid entry" 
done 
# set customer for dir
echo "What customer?(as it is spelt in bitbucket, case sensitive)"
while [ $customer == '' ] 
do
        read -p "What customer?(as it is spelt in bitbucket, case sensitive): " customer 
        echo "Enter a valid entry" 
done
# Enter the production IP
while [ $PROD_HOST == '' ] 
do
        read -p "Enter the production IP address: " PROD_HOST 
        echo "Enter a valid entry" 
done
1>$OUTPUT 2>$ERROR
# navigate to customer dir
cd /home/iris/git/$customer
# make the directories
mkdir qa_export
cd qa_export
mkdir -v tsar
mkdir -v master_data
cd tsar
1>>$OUTPUT 2>>$ERROR
# export tsar from QA
exportTSAR -file ./tsar.tsar
jar -xvf tsar.tsar
1>>$OUTPUT 2>>$ERROR
cd ../
cd master_data
# export master data from QA
exportMasterData -outputDir=. -vocabularyType=all -hierarchyType=all
1>>$OUTPUT 2>>$ERROR
# add new files to git and review then commit
echo "Adding tsar and master data to staging"
git add tsar
git add master_data
git status
echo "Do you want to commit the changes?(Y/N)"
read -s $i
if [$i -ne "N"]
then 
	git commit --author "$firstname $lastname <$firstname.$lastname@frequentz.com>"
	git push
fi
1>>$OUTPUT 2>>$ERROR
# connect to production server and run the prod_validation script
echo "Connecting to production to run production validation"
ssh -o StrictHostKeyChecking=no -l $SSH  "./home/iris/git/qms/frequentz/validation/dscsa/prod_validation.sh"
echo "Validation script has been completed"i
cat $ERROR
exit
