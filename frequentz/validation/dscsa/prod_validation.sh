  #!/bin/bash 
# Production Validation script
# Set environment
. /opt/mqm/bin/setmqenv

if [ "$IRIS_HOME" != "" ]
then
        export TS_HOME=$IRIS_HOME
fi

if [ ! -d logs ]
then
        mkdir logs
fi

OUTPUT=logs/validation/$0.out
ERROR=logs/validation/$0.err

#if customer dir doesn't exist create it
if [ -e /home/iris/git/$customer]
then
        cd /home/iris/git/$customer;
else
        mkdir -p /home/iris/git/$customer
        cd /home/iris/git/$customer;
fi
# pull updates from git
git pull;
# run the deploy script with qa params
./deploy.sh -qa
1>$OUTPUT 2>>$ERROR
# add the logs to git
git add logs/production/prod_import_deploy.*
git status
echo "Do you want to commit the changes?(Y/N)"
read -s $i
if [$i -ne "N"]
then
        git commit --author "$firstname $lastname <$firstname.$lastname@frequentz.com>"
fi
1>>$OUTPUT 2>>$ERROR
# make a dirs for production exports from IRIS
mkdir prod_export
cd prod_export
mkdir tsar
mkdir master_data
cd tsar
## export tsar
exportTSAR -file tsar.tsar
jar -xvf tsar.tsar
rm tsar.tsar
1>>$OUTPUT 2>>$ERROR
# Export all master data into the "master_data" directory:
exportMasterData -outputDir=/home/iris/git/$customer/prod_export/master_data -vocabularyType=all -hierarchyType=all
1>>$OUTPUT 2>>$ERROR
git add tsar
git add master_data
git status
echo "Do you want to commit the changes?(Y/N)"
read -s $i
if [$i -ne "N"]
then
        git commit --author "$firstname $lastname <$firstname.$lastname@frequentz.com>"
fi
1>>$OUTPUT 2>>$ERROR
# You'll be prompted to enter a commit comment. Use something like "Export from Breckenridge Production instance."
#Compare the export from QA to the export from Production using the diff command. Redirect the result to a file prod_export/directory_tree_diff.out:
diff --brief -Nr qa_export/ prod_export/ > prod_export/directory_tree_diff.out
echo "Perform individual comparisons on any files listed to find out if there are real differences or if the differences are only related to changed internal (database) IDs or different ordering. Real differences indicate potential problems and should be addressed."
sleep 10s
# Echo the file to view the differences
echo prod_export/directory_tree_diff.out
# Stage the "directory_tree_diff.out" file
# Commit the current changes using your User ID
echo "Do you want to stage and commit the changes?(Y/N"
read -s $i
if [$i -ne "N"]
then
	git add prod_export/directory_tree_diff.out
	git commit --author "$firstname $lastname <$firstname.$lastname@frequentz.com>"
git push
fi
1>>$OUTPUT 2>>$ERROR
cat $ERROR
exit

