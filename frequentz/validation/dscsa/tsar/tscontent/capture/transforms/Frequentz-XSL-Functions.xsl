<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xmlfn="http://xml.frequentz.com/function"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:java="http://xml.frequentz.com/java">

    <xsl:function name="xmlfn:getMDMAttributeValue" as="xs:string">
        <xsl:param name="vocabularyId" />
        <xsl:param name="vocabularyElementId" />
        <xsl:param name="vocabularyElementAttributeId" />
        <xsl:value-of select="java:getMDMAttributeValue($vocabularyId, $vocabularyElementId, $vocabularyElementAttributeId)" />
    </xsl:function>

</xsl:stylesheet>