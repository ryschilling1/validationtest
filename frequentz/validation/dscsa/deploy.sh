. /opt/mqm/bin/setmqenv
if [ "$IRIS_HOME" != "" ]
then
	export TS_HOME=$IRIS_HOME
fi
if [ ! -d logs ]
then
        mkdir logs
fi
OUTPUT=logs/production/$0.out
ERROR=logs/production/$0.err

# prompt for WAS password
if [ "$was_admin_password" == "" ]
then
	echo "Please type in the was_admin_password"
	read -s was_admin_password
fi
# function to compare the contents of two dir then copy
copier()
{
	SOURCE=$1
	shift
	DESTINATION=$1
	shift
	for i in $@
	do
		echo ${i}
		diff -b $SOURCE/${i} $DESTINATION/${i}
		if [ $? -ne 0 ]
		then
			echo "Press <<ENTER>> to continue."
			read
		fi
		cp -f $SOURCE/${i} $DESTINATION/${i}
	done
}
date 1>$OUTPUT 2>$ERROR
# set Auditing to true
sed --in-place "s/AuditingEnabled>false</AuditingEnabled>true</" $TS_HOME/etc/RFIDICServer.xml
# remove maximum tag per request and replace with blocks per random allocation
grep "<blocksPerRandomAllocation>" $TS_HOME/etc/RFIDICServer.xml
if [ $? -ne 0 ]
then
	sed --in-place "s/<maximumTagsPerRequest>/<blocksPerRandomAllocation>99<\/blocksPerRandomAllocation><maximumTagsPerRequest>/" $TS_HOME/etc/RFIDICServer.xml
fi
# copy log to prod with version info
copier etc/ $TS_HOME/etc/ log.xml
../solutions/bin/versionInfo.sh 1>>$OUTPUT 2>>$ERROR
# move samples over to prod
mkdir $TS_HOME/samples/audit/archive
mv $TS_HOME/samples/audit/*.xml $TS_HOME/samples/audit/archive
# unzip TSAR
TSAR=tsar.tsar
# if QA validation, change dir
if [ $1 -e "-qa" ]
then
        cd qa_export/tsar
else
        cd tsar
fi
# un pack the tsar
jar cvf $TSAR *
# change if running this for QA validation
if [ $1 -e "-qa" ]
then 
	cd ../../
else
	cd ../
fi
# check for cluster or not
DESTINATIONDIR=$WAS_HOME/profiles/Dmgr01/config/cells/$WAS_CELL/applications/
if [ ! -d $DESTINATIONDIR ]
then
	DESTINATIONDIR=$CONFIG_ROOT/cells/$WAS_CELL/applications/
fi
echo $DESTINATIONDIR
SOURCEDIR=security
cp $SOURCEDIR/com.frequentz.iris.snmApp/ibm-application-bnd.xml $DESTINATIONDIR/com.frequentz.iris.snmApp.ear/deployments/com.frequentz.iris.snmApp/META-INF
cp $SOURCEDIR/com.frequentz.iris.dscsa.webEAR/ibm-application-bnd.xml $DESTINATIONDIR/com.frequentz.iris.dscsa.webEAR.ear/deployments/com.frequentz.iris.dscsa.webEAR/META-INF
cp $SOURCEDIR/com.frequentz.iris.rework.webEAR/ibm-application-bnd.xml $DESTINATIONDIR/com.frequentz.iris.rework.webEAR.ear/deployments/com.frequentz.iris.rework.webEAR/META-INF
cp $SOURCEDIR/com.frequentz.iris.web.ui.ear/ibm-application-bnd.xml $DESTINATIONDIR/com.frequentz.iris.web.ui.ear.ear/deployments/com.frequentz.iris.web.ui.ear/META-INF
cp $SOURCEDIR/com.frequentz.iris.irisApp/ibm-application-bnd.xml $DESTINATIONDIR/com.frequentz.iris.irisApp.ear/deployments/com.frequentz.iris.irisApp/META-INF
# remove the subscription
# import the tsar 
# change if running this for QA validation
if [ $1 -e "-qa" ]
then
        time echo $was_admin_password | $TS_HOME/bin/importReplaceTSAR -file qa_export/tsar/$TSAR 1>>$OUTPUT 2>>$ERROR
else
        time echo $was_admin_password | $TS_HOME/bin/importReplaceTSAR -file tsar/$TSAR 1>>$OUTPUT 2>>$ERROR

fi
# import tsar, if -qa is passed then run the QA export tsar
if [ $? -ne 0 ]
then
	if [ $1 -e "-qa" ]
	then
		# do this again if there was an error removing the subscription
		time echo $was_admin_password | $TS_HOME/bin/importReplaceTSAR -file qa_export/tsar/$TSAR 1>>$OUTPUT 2>>$ERROR
		# need to start iris because the previous deploy failed
		time echo $was_admin_password | $TS_HOME/bin/startIRIS -A -S=ENGINE,UI,SERIALID,CAPTURE

	else
                # do this again if there was an error removing the subscription
                time echo $was_admin_password | $TS_HOME/bin/importReplaceTSAR -file tsar/$TSAR 1>>$OUTPUT 2>>$ERROR
                # need to start iris because the previous deploy failed		
		time echo $was_admin_password | $TS_HOME/bin/startIRIS -A -S=ENGINE,UI,SERIALID,CAPTURE
	fi
fi
# import masterdata, if -qa is passed then run the QA export master data
if [ $? -eq 0 ]
then
        if [ $1 -e "-qa" ]
	then
                rm qa_export/tsar/$TSAR
	        sql/check_additionalTradeItemIdentificationType.sh 1>>$OUTPUT 2>>$ERROR
       		time echo $was_admin_password | $TS_HOME/bin/importMasterData -url_prefix http://`hostname`:9080 qa_export/data/masterdata/*.xml 1>>$OUTPUT 2>>$ERROR

	else
		rm tsar/$TSAR
		sql/check_additionalTradeItemIdentificationType.sh 1>>$OUTPUT 2>>$ERROR
		time echo $was_admin_password | $TS_HOME/bin/importMasterData -url_prefix http://`hostname`:9080 data/masterdata/*.xml 1>>$OUTPUT 2>>$ERROR
fi
cat $ERROR
